﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;

namespace NimbusFox.ModularGreenhouses.Components; 

public class SpeedComponent {
    public double Chance { get; }

    public SpeedComponent(Blob config) {
        Chance = config.GetDouble("chance", 0.25);
    }
}