﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel;

namespace NimbusFox.ModularGreenhouses.Components; 

public class ModularGreenhousesComponent {
    public Plukit.Base.Components Components { get; }
    public ModularGreenhousesComponent(Blob config) {
        Components = new Plukit.Base.Components();
        foreach (var entry in config.KeyValueIteratable) {
            if (entry.Value.Kind == BlobEntryKind.Blob) {
                if (GreenhouseHook.Instance.GreenhouseComponentBuilders.TryGetValue(entry.Key, out var builder)) {
                    Components.SetTypeless(builder.Instance(entry.Value.Blob()));
                }
            }
        }
    }
}