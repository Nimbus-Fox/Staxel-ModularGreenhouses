﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;

namespace NimbusFox.ModularGreenhouses.Components; 

public class ModularGreenhousesPlantComponent {
    public string GoldenPlant { get; }

    public ModularGreenhousesPlantComponent(Blob config) {
        GoldenPlant = config.GetString("goldenPlant");
    }
}