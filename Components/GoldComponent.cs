﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;

namespace NimbusFox.ModularGreenhouses.Components; 

public class GoldComponent {
    public double Chance { get; }

    public GoldComponent(Blob config) {
        Chance = config.GetDouble("chance", 0.1);
    }
}