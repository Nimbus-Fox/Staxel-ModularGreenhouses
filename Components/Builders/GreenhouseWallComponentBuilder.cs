﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Core;

namespace NimbusFox.ModularGreenhouses.Components.Builders; 

public class GreenhouseWallComponentBuilder : IComponentBuilder {
    public string Kind() {
        return "modularGreenhouseWall";
    }

    public object Instance(Blob config) {
        return new GreenhouseWallComponent();
    }
}