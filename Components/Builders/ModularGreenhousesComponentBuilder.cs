﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Core;

namespace NimbusFox.ModularGreenhouses.Components.Builders; 

public class ModularGreenhousesComponentBuilder : IComponentBuilder {
    public string Kind() {
        return "modularGreenhouses";
    }

    public object Instance(Blob config) {
        return new ModularGreenhousesComponent(config);
    }
}