﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Core;
using Staxel.Tiles;

namespace NimbusFox.ModularGreenhouses.Components.Builders; 

public class SpeedComponentBuilder : IGreenhouseComponentBuilder {
    public string Kind() {
        return "fast";
    }

    public object Instance(Blob config) {
        return new SpeedComponent(config);
    }
}