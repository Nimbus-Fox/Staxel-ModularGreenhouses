﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;

namespace NimbusFox.ModularGreenhouses; 

public interface IGreenhouseComponentBuilder {
    string Kind();
    object Instance(Blob config);
}