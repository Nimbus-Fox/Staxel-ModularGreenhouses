﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates.Docks;

namespace NimbusFox.ModularGreenhouses.Entities.GreenhouseController; 

public class GreenhouseControllerTileStateEntityBuilder : IEntityPainterBuilder, IEntityLogicBuilder2, IEntityLogicBuilder {
    public void Load() { }
    public string Kind => ControllerCode;
    public bool IsTileStateEntityKind() {
        return true;
    }

    public static string ControllerCode => "nimbusFox.modularGreenhouses.tileStateEntity.controller";

    EntityLogic IEntityLogicBuilder.Instance(Entity entity, bool server) {
        return new GreenhouseControllerTileEntityLogic(entity);
    }

    EntityPainter IEntityPainterBuilder.Instance() {
        return new DockTileStateEntityPainter(null);
    }

    public static Entity Spawn(Vector3I position, EntityUniverseFacade universe, Tile tile) {
        var entity = new Entity(universe.AllocateNewEntityId(), false, ControllerCode, true);

        var blob = BlobAllocator.Blob(true);
        blob.SetString("kind", ControllerCode);
        blob.FetchBlob("location").SetVector3I(position);
        blob.SetString("tile", tile.Configuration.Code);
        blob.SetLong("variant", tile.Variant());
        blob.FetchBlob("velocity").SetVector3D(Vector3D.Zero);

        entity.Construct(blob, universe);

        universe.AddEntity(entity);

        return entity;
    }
}