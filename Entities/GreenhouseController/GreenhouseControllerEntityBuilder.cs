﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.ModularGreenhouses.Entities.GreenhouseController; 

public class GreenhouseControllerEntityBuilder : IEntityPainterBuilder, IEntityLogicBuilder2, IEntityLogicBuilder {
    public void Load() { }
    public string Kind => ControllerCode;
    public bool IsTileStateEntityKind() {
        return false;
    }

    public static string ControllerCode => "nimbusfox.modulargreenhouses.entity.controller";

    EntityLogic IEntityLogicBuilder.Instance(Entity entity, bool server) {
        return new GreenhouseControllerEntityLogic(entity);
    }

    EntityPainter IEntityPainterBuilder.Instance() {
        return new GreenhouseControllerEntityPainter();
    }

    public static Entity Spawn(Vector3I position, EntityUniverseFacade universe) {
        var entity = new Entity(universe.AllocateNewEntityId(), false, ControllerCode, true);

        var blob = BlobAllocator.Blob(true);
        blob.SetString("kind", ControllerCode);
        blob.FetchBlob("location").SetVector3I(position);

        entity.Construct(blob, universe);

        universe.AddEntity(entity);

        return entity;
    }
}