﻿using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.ModularGreenhouses.Entities.GreenhouseController; 

public class GreenhouseControllerTileStateBuilder : ITileStateBuilder {
    public const string KindCode = "nimbusFox.modularGreenhouses.tileState.controller";
    /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
    public void Dispose() { }
    public void Load() { }
    public string Kind() {
        return KindCode;
    }

    public Entity Instance(Vector3I location, Tile tile, Universe universe) {
        return GreenhouseControllerTileStateEntityBuilder.Spawn(location, universe, tile);
    }
}