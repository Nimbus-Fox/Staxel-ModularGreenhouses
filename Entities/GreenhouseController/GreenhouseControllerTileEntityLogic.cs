﻿using Plukit.Base;
using Staxel.Logic;
using Staxel.TileStates.Docks;

namespace NimbusFox.ModularGreenhouses.Entities.GreenhouseController; 

public class GreenhouseControllerTileEntityLogic : DockTileStateEntityLogic {

    private Entity _childEntity;
    private GreenhouseControllerEntityLogic _childLogic;

    public GreenhouseControllerTileEntityLogic(Entity entity) : base(entity) {
    }

    public override void PreUpdate(Timestep timestep, EntityUniverseFacade universe) {
        base.PreUpdate(timestep, universe);

        if (_childEntity == null) {
            var target = universe.FindNearestEntityInRange(Location.ToVector3D(), 1, entity => {
                if (entity.IsDisposed) {
                    return false;
                }

                return entity.Logic is GreenhouseControllerEntityLogic;
            });

            _childEntity = target ?? GreenhouseControllerEntityBuilder.Spawn(Location, universe);

            if (_childEntity.Logic is GreenhouseControllerEntityLogic logic) {
                _childLogic = logic;
            }
        }
    }

    public override void StorePersistenceData(Blob blob) {
        blob.SetLong("childEntity", _childEntity.Id.Id);
        base.StorePersistenceData(blob);
    }

    public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) {
        base.RestoreFromPersistedData(data, facade);

        if (data.Contains("childEntity")) {
            if (facade.TryGetEntity(new EntityId(data.GetLong("childEntity")), out var entity)) {
                _childEntity = entity;

                if (_childEntity.Logic is GreenhouseControllerEntityLogic logic) {
                    _childLogic = logic;
                }
            }
        }
    }

    public override string AltInteractVerb() {
        return "Scan";
    }

    public override bool SuppressInteractVerb(Entity interacter) {
        if (interacter.PlayerEntityLogic == null) {
            return base.SuppressInteractVerb(interacter);
        }

        return !interacter.PlayerEntityLogic.LookingAtDockedItem().IsNull() &&
               interacter.PlayerEntityLogic.Inventory().ActiveItem().IsNull() &&
               base.SuppressInteractVerb(interacter);
    }

    public override void Interact(Entity user, EntityUniverseFacade facade, ControlState main, ControlState alt) {
        if (SuppressInteractVerb(user)) {
            base.Interact(user, facade, main, alt);
            return;
        }

        if (alt.DownClick) {
            _childLogic?.StartScan();
        }
    }
}