﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Draw;
using Staxel.Logic;
using Staxel.Rendering;
using Staxel.Voxel;

namespace NimbusFox.ModularGreenhouses.Entities.GreenhouseController; 

public class GreenhouseControllerEntityPainter : EntityPainter {

    private static readonly CompactVertexDrawable Drawable;

    static GreenhouseControllerEntityPainter() {
        Drawable = VoxelLoader.LoadQb(
            GameContext.ContentLoader.ReadStream("mods/ModularGreenhouses/Staxel/GreenhouseError.qb"),
            "mods/ModularGreenhouses/Staxel/GreenhouseError.qb", Vector3I.Zero, Vector3I.MaxValue).BuildVertices();
    }

    protected override void Dispose(bool disposing) { }

    public override void RenderUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade,
        int updateSteps) { }

    public override void ClientUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade) { }
    public override void ClientPostUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade) { }

    public override void BeforeRender(DeviceContext graphics, Vector3D renderOrigin, Entity entity, AvatarController avatarController,
        Timestep renderTimestep) { }

    public override void Render(DeviceContext graphics, ref Matrix4F matrix, Vector3D renderOrigin, Entity entity,
        AvatarController avatarController, Timestep renderTimestep, RenderMode renderMode) {
        if (entity.Logic is GreenhouseControllerEntityLogic logic) {
            if (logic.Invalid) {
                var matrix2 = Matrix4F.Identity.Translate((logic.Current.ToVector3D() - renderOrigin) + new Vector3D(0, 0.5f, 0))
                    .Multiply(ref matrix);
                Drawable.Render(graphics, ref matrix2);
            }
        }
    }

    public override void StartEmote(Entity entity, Timestep renderTimestep, EmoteConfiguration emote) { }
}