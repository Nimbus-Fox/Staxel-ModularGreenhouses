﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.KitsuneToolBox.ChunkLoader.V1;
using NimbusFox.KitsuneToolBox.Structs;
using NimbusFox.ModularGreenhouses.Components;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Farming;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Sky;
using Staxel.Tiles;

namespace NimbusFox.ModularGreenhouses.Entities.GreenhouseController; 

public class GreenhouseControllerEntityLogic : EntityLogic {

    private Blob _constructBlob;

    public bool Scanning { get; private set; }

    public bool Invalid { get; private set; }

    public Vector3I Current { get; private set; } = Vector3I.Zero;

    public VectorCubeI Region { get; private set; } = new VectorCubeI(Vector3I.Zero, Vector3I.Zero);

    public Vector3I Location { get; private set; }

    private readonly Dictionary<int, List<Vector3I>> _scanned = new Dictionary<int, List<Vector3I>>();

    private readonly Entity _entity;

    private bool _needsStore;

    private Vector3I _origin = Vector3I.Zero;

    private Vector3I _last = Vector3I.Zero;

    private int _yIndex = 0;

    public double Temperature { get; private set; } = 20;

    public GreenhouseControllerEntityLogic(Entity entity) {
        _entity = entity;
    }

    public override void PreUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }

    public override void Update(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
        if (Scanning) {
            Scan(entityUniverseFacade);
        }
    }

    public override void PostUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
        if (entityUniverseFacade.TryFetchTileStateEntityLogic(Location, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId,
                out var logic)) {
            if (logic is GreenhouseControllerTileEntityLogic == false) {
                entityUniverseFacade.RemoveEntity(_entity.Id);
                ChunkLoader.RemoveChunk(Location);
            }
        } else {
            entityUniverseFacade.RemoveEntity(_entity.Id);
            ChunkLoader.RemoveChunk(Location);
        }
    }

    public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
        Location = arguments.FetchBlob("location").GetVector3I();

        _entity.Physics.MakePhysicsless();
        _entity.Physics.ForcedPosition(Location.ToVector3D());

        ChunkLoader.AddChunk(Location);

        _constructBlob = BlobAllocator.Blob(true);

        _constructBlob.AssignFrom(arguments);

        _needsStore = true;

        GreenhouseHook.Instance.AddEntityCheck(_entity);
    }
    public override void Bind() { }
    public override bool Interactable() {
        return false;
    }

    public override void Interact(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) { }
    public override bool CanChangeActiveItem() {
        return false;
    }

    public override Heading Heading() {
        return new Heading();
    }

    public override bool IsPersistent() {
        return true;
    }

    public override void Store() {
        if (_needsStore) {
            _needsStore = false;

            _entity.Blob.FetchBlob("currentScan").SetVector3I(Current);
            _entity.Blob.SetBool("scanning", Scanning);
            _entity.Blob.SetBool("invalid", Invalid);
            _entity.Blob.SetDouble("temperature", Temperature);

            _entity.Blob.FetchBlob("region").FetchBlob("start").SetVector3I(Region.Start);
            _entity.Blob.FetchBlob("region").FetchBlob("end").SetVector3I(Region.End);
        }
    }

    public override void StorePersistenceData(Blob data) {
        if (_constructBlob != null) {
            data.FetchBlob("construct").AssignFrom(_constructBlob);
        }

        data.FetchBlob("region").FetchBlob("start").SetVector3I(Region.Start);
        data.FetchBlob("region").FetchBlob("end").SetVector3I(Region.End);
        data.SetDouble("temperature", Temperature);
    }

    public override void Restore() {
        if (_entity.Blob.Contains("currentScan")) {
            Current = _entity.Blob.FetchBlob("currentScan").GetVector3I();
        }

        if (_entity.Blob.Contains("scanning")) {
            Scanning = _entity.Blob.GetBool("scanning");
        }

        if (_entity.Blob.Contains("invalid")) {
            Invalid = _entity.Blob.GetBool("invalid");
        }

        if (_entity.Blob.Contains("region")) {
            var region = _entity.Blob.FetchBlob("region");

            Region = new VectorCubeI(region.FetchBlob("start").GetVector3I(), region.FetchBlob("end").GetVector3I());
        }

        if (_entity.Blob.Contains("temperature")) {
            Temperature = _entity.Blob.GetDouble("temperature");
        }
    }

    public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) {
        if (data.Contains("construct")) {
            Construct(data.FetchBlob("construct"), facade);
        }

        if (data.Contains("region")) {
            var region = data.FetchBlob("region");

            Region = new VectorCubeI(region.FetchBlob("start").GetVector3I(), region.FetchBlob("end").GetVector3I());
        }

        if (data.Contains("temperature")) {
            Temperature = data.GetDouble("temperature");
        }
    }
    public override bool IsCollidable() {
        return false;
    }

    public void StartScan() {
        if (!Scanning) {
            Scanning = true;
            Current = Location;
            Region = new VectorCubeI(Vector3I.Zero, Vector3I.Zero);
            _needsStore = true;
            _scanned.Clear();
            _origin = Vector3I.Zero;
        }
    }

    private void Scan(EntityUniverseFacade entityUniverseFacade) {
        var scan = new[] {
            Current + new Vector3I(1, 0, 0),
            Current + new Vector3I(0, 0, 1),
            Current - new Vector3I(1, 0, 0),
            Current - new Vector3I(0, 0, 1)
        };

        Invalid = true;

        if (!_scanned.ContainsKey(_yIndex)) {
            _scanned.Add(_yIndex, new List<Vector3I>());
        }

        if (Current == _origin && _scanned[_yIndex].Count > 1) {
            Invalid = false;
            if (entityUniverseFacade.ReadTile(Current + new Vector3I(0, 1, 0),
                    TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var aboveTile)) {

                if (aboveTile.Configuration.Components.Contains<GreenhouseWallComponent>()) {
                    Current += new Vector3I(0, 1, 0);
                    _origin = Vector3I.Zero;
                    _needsStore = true;
                    _yIndex++;
                    return;
                }

                if (aboveTile.Configuration.Code == Constants.CompoundCode) {
                    if (entityUniverseFacade.FindReadCompoundTileCore(Current + new Vector3I(0, 1, 0),
                            TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out _, out var aboveCompound)) {
                        if (aboveCompound.Configuration.Components
                            .Contains<GreenhouseWallComponent>()) {
                            Current += new Vector3I(0, 1, 0);
                            _origin = Vector3I.Zero;
                            _needsStore = true;
                            _yIndex++;
                            return;
                        }
                    }
                }
            }

            _needsStore = true;

            Scanning = false;

            var max = Vector3I.Max(_scanned.Values.SelectMany(x => x.ToArray()).ToArray());

            var min = Vector3I.Min(_scanned.Values.SelectMany(x => x.ToArray()).ToArray());

            Region = new VectorCubeI(min, max);

            return;
        }

        foreach (var current in scan) {
            if (current == _origin) {
                Invalid = false;
                Current = current;
                break;
            }
            if (current == _last || _scanned[_yIndex].Contains(current)) {
                continue;
            }

            if (entityUniverseFacade.ReadTile(current, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tile)) {
                if (tile.Configuration.Code == Constants.CompoundCode) {
                    if (entityUniverseFacade.FindReadCompoundTileCore(current,
                            TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId,
                            out var core, out var compoundTile)) {

                        if (compoundTile.Configuration.Components.Contains<GreenhouseWallComponent>()) {
                            Invalid = false;
                            _last = Current;
                            _scanned[_yIndex].Add(Current);
                            Current = current;
                            _needsStore = true;

                            if (_origin == Vector3I.Zero) {
                                _origin = current;
                            }

                            continue;
                        }
                    }
                }

                if (tile.Configuration.Components.Contains<GreenhouseWallComponent>()) {
                    Invalid = false;
                    _last = Current;
                    _scanned[_yIndex].Add(Current);
                    Current = current;
                    _needsStore = true;

                    if (_origin == Vector3I.Zero) {
                        _origin = current;
                    }

                    break;
                }
            }
        }

        if (Invalid) {
            Scanning = false;
            _needsStore = true;
        }
    }
}