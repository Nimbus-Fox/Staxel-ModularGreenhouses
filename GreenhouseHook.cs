﻿using System;
using System.Collections.Generic;
using System.Reflection;
using HarmonyLib;
using NimbusFox.KitsuneToolBox.Helpers.V1;
using NimbusFox.KitsuneToolBox.Interfaces;
using NimbusFox.KitsuneToolBox.ToolBox.V1;
using NimbusFox.ModularGreenhouses.Entities.GreenhouseController;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Farming;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Notifications;
using Staxel.Tiles;

namespace NimbusFox.ModularGreenhouses; 

public class GreenhouseHook : IToolBoxHookPlant {
    internal static readonly ToolBox ToolBox;
    internal static GreenhouseHook Instance { get; private set; }

    internal readonly List<Entity> CheckEntities = new List<Entity>();

    internal readonly Dictionary<string, IGreenhouseComponentBuilder> GreenhouseComponentBuilders;

    static GreenhouseHook() {
        ModHelper.CheckModIsInstalled("ModularGreenhouses", "KitsuneToolBox");
        ToolBox = new ToolBox("NimbusFox", "ModularGreenhouses");
    }

    public GreenhouseHook() {
        Instance = this;
        GreenhouseComponentBuilders = new Dictionary<string, IGreenhouseComponentBuilder>();
    }
        
    public void Dispose() {
            
    }

    public void GameContextInitializeInit() {
        foreach (var builder in HelperFunctions.GetTypesUsingBase<IGreenhouseComponentBuilder>()) {
            var build = (IGreenhouseComponentBuilder) Activator.CreateInstance(builder);
            GreenhouseComponentBuilders.Add(build.Kind(), build);
        }
    }

    public void GameContextInitializeBefore() {
            
    }

    public void GameContextInitializeAfter() {
            
    }

    public void GameContextDeinitialize() {
            
    }

    public void GameContextReloadBefore() {
            
    }

    public void GameContextReloadAfter() {
            
    }

    public void UniverseUpdateBefore(Universe universe, Timestep step) {
            
    }

    public void UniverseUpdateAfter() {
            
    }

    public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
        return true;
    }

    public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
        return true;
    }

    public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
        return true;
    }

    public void ClientContextInitializeInit() {
            
    }

    public void ClientContextInitializeBefore() {
            
    }

    public void ClientContextInitializeAfter() {
            
    }

    public void ClientContextDeinitialize() {
            
    }

    public void ClientContextReloadBefore() {
            
    }

    public void ClientContextReloadAfter() {
            
    }

    public void CleanupOldSession() {
            
    }

    public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
        return true;
    }

    public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
        return true;
    }

    public void OnPlayerConnect(Entity entity) {
            
    }

    public void OnPlayerDisconnect(Entity entity) {
            
    }

    public bool CanPlant(bool lastResult, Entity entity, Vector3I cursor, Tile tile, EntityUniverseFacade facade, ref string reason,
        ref NotificationParams parameters) {
        if (!reason.Contains("WrongSeason")) {
            return lastResult;
        }

        var insideCursor = cursor + new Vector3I(0, 1, 0);

        if (facade.ReadTile(cursor, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld,
                EntityId.NullEntityId, out var lowerTile)) {
            if (!GameContext.FarmingDatabase.IsMaterialTilled(lowerTile.Configuration)) {
                reason = "staxel.notifications.needTilledTile";
                parameters = NotificationParams.EmptyParams;
                return false;
            }
        }

        if (facade.ReadTile(insideCursor, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld,
                EntityId.NullEntityId, out var aboveTile)) {
            if (aboveTile.Configuration.Code != Constants.SkyCode) {
                parameters = NotificationParams.EmptyParams;
                return false;
            }
        }

        foreach (var check in CheckEntities) {
            if (check.Logic is GreenhouseControllerEntityLogic logic) {
                if (!logic.Invalid) {
                    if (logic.Region.GetTileCount() > 0) {
                        if (logic.Region.Contains(insideCursor)) {
                            reason = "";
                            parameters = NotificationParams.EmptyParams;
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public void OnHarvest(Entity entity, EntityUniverseFacade facade, Vector3I soilLocation, List<ItemStack> items, ChunkFetchKind kind) {
            
    }
        
    private static readonly MethodBase FarmDatabaseTileUpdated = AccessTools.Method(typeof(FarmingDatabase), "TileUpdated");
    private static readonly MethodBase FarmDatabaseChangeTile = AccessTools.Method(typeof(FarmingDatabase), "ChangeTile");
    private static readonly MethodBase FarmDatabaseMakeUnwateredMaterial = AccessTools.Method(typeof(FarmingDatabase), "MakeUnwateredMaterial");
    private static readonly MethodBase FarmDatabaseMakeWateredMaterial = AccessTools.Method(typeof(FarmingDatabase), "MakeWateredMaterial");
    private static readonly MethodBase FarmDatabaseIsMaterialWaterable = AccessTools.Method(typeof(FarmingDatabase), "IsMaterialWaterable");

    public void OnDailyPlantVisit(Blob plantBlob, Vector3I plantLocation, Tile plantTile, PlantConfiguration plantConfig,
        Vector3I soilLocation, Tile soilTile, EntityUniverseFacade universe, bool weatherWatered, bool sprinklerWatered,
        out bool runBaseDailyVisitFunction) {
        runBaseDailyVisitFunction = true;
            
        var day = universe.DayNightCycle().Day;
        var season = universe.DayNightCycle().GetSeason();
        var watered = GameContext.FarmingDatabase.IsWateredMaterial(soilTile.Configuration);
        var plantedInGreenhouse = false;

        foreach (var check in CheckEntities) {
            if (check.Logic is GreenhouseControllerEntityLogic logic) {
                if (!logic.Invalid) {
                    if (logic.Region.GetTileCount() > 0) {
                        if (logic.Region.Contains(plantLocation)) {
                            plantedInGreenhouse = true;
                            break;
                        }
                    }
                }
            }
        }

        if (!plantedInGreenhouse) {
            return;
        }

        if (GameContext.PlantDatabase.IsGrowable(plantTile)) {
            var plantConfiguration = GameContext.PlantDatabase.GetByTile(plantTile.Configuration);
            var requiresWatering = GameContext.PlantDatabase.RequiresWatering(plantTile);
            var lastChangedDay = plantBlob.GetLong("day", -1L);

            if (lastChangedDay == -1 || lastChangedDay > day) {
                lastChangedDay = day - 1;
                FarmDatabaseTileUpdated.Invoke(GameContext.FarmingDatabase, new object[] {
                    plantLocation,
                    lastChangedDay,
                    plantConfiguration.GatherableIsHarvestable,
                    season
                });
            }

            var totalDays = day - lastChangedDay;
            var growthDays = 0;
            var daysPassed = 1;
            for (var i = 0; i < totalDays; i++) {
                Vector2I window;
                if (!watered && !sprinklerWatered && requiresWatering && plantConfiguration.CanWilt(plantTile, out window)) {
                    if (GameContext.RandomSource.Next(window.X, window.Y) <= daysPassed - growthDays &&
                        (bool)FarmDatabaseChangeTile.Invoke(GameContext.FarmingDatabase, new object[] {
                            plantLocation,
                            plantConfiguration.MakeWiltedTile(plantTile),
                            universe,
                            plantConfiguration.GatherableIsHarvestable,
                            null,
                            TileAccessFlags.IgnoreEntities
                        })) {
                        break;
                    }
                }

                if (!watered && !sprinklerWatered && requiresWatering && plantConfiguration.CanWither(plantTile, out window)) {
                    if (GameContext.RandomSource.Next(window.X, window.Y) <= daysPassed - growthDays &&
                        (bool)FarmDatabaseChangeTile.Invoke(GameContext.FarmingDatabase, new object[] {
                            plantLocation,
                            plantConfiguration.MakeWitheredTile(plantTile),
                            universe,
                            plantConfiguration.GatherableIsHarvestable,
                            null,
                            TileAccessFlags.IgnoreEntities
                        })) {
                        break;
                    }
                }

                if (!watered && !sprinklerWatered && requiresWatering &&
                    !plantConfiguration.CanWilt(plantTile, out _) &&
                    !plantConfiguration.CanWither(plantTile, out _)) {
                    FarmDatabaseTileUpdated.Invoke(GameContext.FarmingDatabase, new object[] {
                        plantLocation,
                        day,
                        plantConfiguration.GatherableIsHarvestable,
                        season
                    });
                    break;
                }

                if ((watered || sprinklerWatered ||
                     (plantConfiguration.GatherableIsHarvestable && GameContext.RandomSource.NextBool())) &&
                    plantConfiguration.CanGrow(plantTile, SeasonHelper.FromInt(season), out window)) {
                    var next = GameContext.RandomSource.Next(window.X, window.Y);
                    var nextPlant = plantConfiguration.MakeGrowTile(plantTile, GameContext.RandomSource);



                    if (next <= daysPassed - growthDays &&
                        (bool)FarmDatabaseChangeTile.Invoke(GameContext.FarmingDatabase, new object[] {
                            plantLocation,
                            nextPlant,
                            universe,
                            plantConfiguration.GatherableIsHarvestable,
                            null,
                            TileAccessFlags.IgnoreEntities
                        })) {
                        growthDays += next;
                        if (!universe.ReadTile(plantLocation, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out plantTile)) {
                            runBaseDailyVisitFunction = false;
                            return;
                        }
                    }
                }

                daysPassed++;
            }

            if (watered || sprinklerWatered) {
                var unwarteredTile =
                    (Tile)FarmDatabaseMakeUnwateredMaterial.Invoke(GameContext.FarmingDatabase,
                        new object[] { soilTile });
                FarmDatabaseChangeTile.Invoke(GameContext.FarmingDatabase, new object[] {
                    soilLocation,
                    unwarteredTile,
                    universe,
                    false,
                    null,
                    TileAccessFlags.IgnoreEntities
                });
            }
        }

        runBaseDailyVisitFunction = false;
    }

    public void OnCheckPlantGrowth(Entity entity, EntityUniverseFacade facade, Vector3I plantLocation, Tile plantTile,
        PlantConfiguration plantConfig, Vector3I soilLocation, Tile soilTile, out bool runBaseCheckPlantGrowthFunction) {
        var playerEntityLogic = entity.PlayerEntityLogic;
        var tile = default(Tile);
        PlantConfiguration config;
        var cursor = default(Vector3I);
        var adjacent = default(Vector3I);
        var core = default(Vector3I);

        if (playerEntityLogic != null && playerEntityLogic.LookingAtTile(out cursor, out adjacent) && facade.FindReadCompoundTileCore(cursor, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out core, out tile) && GameContext.PlantDatabase.TryGetByTile(tile, out config)) {
            Season season = SeasonHelper.FromInt(facade.DayNightCycle().GetSeason());
            var livesInThisSeason = config.LivesInSeason(season);
            var plantedInGreenhouse = false;

            foreach (var check in CheckEntities) {
                if (check.Logic is GreenhouseControllerEntityLogic logic) {
                    if (!logic.Invalid) {
                        if (logic.Region.GetTileCount() > 0) {
                            if (logic.Region.Contains(cursor)) {
                                plantedInGreenhouse = true;
                                break;
                            }
                        }
                    }
                }
            }

            Notification notif;
            if (config.IsWitheredTile(tile)) {
                notif = ((!livesInThisSeason && !plantedInGreenhouse) ? GameContext.NotificationDatabase.CreateNotificationFromCode("staxel.notifications.checkPlantGrowth.witheredSeason", entity.Step, NotificationParams.CreateFromTranslation(season.GetCode()), false) : GameContext.NotificationDatabase.CreateNotificationFromCode("staxel.notifications.checkPlantGrowth.withered", entity.Step, NotificationParams.EmptyParams, false));
            } else if (config.IsWiltedTile(tile)) {
                notif = GameContext.NotificationDatabase.CreateNotificationFromCode("staxel.notifications.checkPlantGrowth.wilted", entity.Step, NotificationParams.EmptyParams, false);
            } else if (!livesInThisSeason && !plantedInGreenhouse) {
                notif = GameContext.NotificationDatabase.CreateNotificationFromCode("staxel.notifications.checkPlantGrowth.season", entity.Step, NotificationParams.CreateFromTranslation(season.GetCode()), false);
            } else {
                var percentage = config.GetGrowthPercentage(tile, config);
                var description = (!(percentage < 0.1f)) ? ((!(percentage < 1f)) ? "hintText.checkPlantGrowth.fruited" : "hintText.checkPlantGrowth.growing") : "hintText.checkPlantGrowth.seed";
                notif = GameContext.NotificationDatabase.CreateNotificationFromCode("staxel.notifications.checkPlantGrowth", entity.Step, NotificationParams.CreateFromTranslation(description), false);
            }
            playerEntityLogic.ShowNotification(notif);
        }
        entity.Logic?.ActionFacade?.NoNextAction();
        runBaseCheckPlantGrowthFunction = false;
    }

    public void AddEntityCheck(Entity entity) {
        if (HelperFunctions.IsServer() && !CheckEntities.Contains(entity) && !entity.IsDisposed && !entity.Removed) {
            CheckEntities.Add(entity);
        }
    }
}